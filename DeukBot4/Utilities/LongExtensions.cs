﻿namespace DeukBot4.Utilities
{
    public static class LongExtensions
    {
        public static ulong ToUlong(this long l)
        {
            return unchecked((ulong)(l - long.MinValue));
        }

        public static long ToLong(this ulong l)
        {
            return unchecked((long)l + long.MinValue);
        }
    }
}