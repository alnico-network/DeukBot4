﻿using System;
using System.Linq;
using System.Text;

namespace DeukBot4.Utilities
{
    public static class TimespanHelper
    {
        public static TimeSpan? Parse(string s)
        {
            var timeIndicator = s.Last();
            var numberStr     = s.Remove(s.Length - 1, 1);
            if (!float.TryParse(numberStr, out var number))
            {
                return null;
            }

            switch (timeIndicator)
            {
                case 's':
                    return TimeSpan.FromSeconds(number);
                case 'm':
                    return TimeSpan.FromMinutes(number);
                case 'h':
                    return TimeSpan.FromHours(number);
                case 'd':
                    return TimeSpan.FromDays(number);
                default:
                    return null;
            }
        }
        public static string ToPrettyFormat(this TimeSpan span) {

            if (span == TimeSpan.Zero) return "0 minutes";

            var sb = new StringBuilder();
            if (span.Days > 0)
                sb.AppendFormat("{0} day{1} ", span.Days, span.Days > 1 ? "s" : String.Empty);
            if (span.Hours > 0)
                sb.AppendFormat("{0} hour{1} ", span.Hours, span.Hours > 1 ? "s" : String.Empty);
            if (span.Minutes > 0)
                sb.AppendFormat("{0} minute{1} ", span.Minutes, span.Minutes > 1 ? "s" : String.Empty);
            sb.TrimEnd();
            return sb.ToString();

        }

    }
}