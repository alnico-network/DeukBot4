﻿namespace DeukBot4.MessageHandlers.Permissions
{
    public enum PermissionLevel : sbyte
    {
        Banned = -10,
        Bot = -5,
        Everyone = 0,
        Helper = 20,
        Moderator = 40,
        Admin = 60,
        ServerOwner = 80,
        BotCreator = 100
    }
}