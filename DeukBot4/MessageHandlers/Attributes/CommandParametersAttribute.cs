﻿using System;
using DeukBot4.MessageHandlers.CommandHandler.RequestStructure;

namespace DeukBot4.MessageHandlers
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class CommandParametersAttribute : Attribute
    {
        public ParameterMatcher.ParameterType[] Types { get; }

        public CommandParametersAttribute(ParameterMatcher.ParameterType[] types)
        {
            Types = types;
        }

        public CommandParametersAttribute(ParameterMatcher.ParameterType type)
        {
            Types = new []{type};
        }
        public CommandParametersAttribute(ParameterMatcher.ParameterType type, ParameterMatcher.ParameterType type2)
        {
            Types = new []{type, type2};
        }
        public CommandParametersAttribute(ParameterMatcher.ParameterType type, ParameterMatcher.ParameterType type2,
            ParameterMatcher.ParameterType type3)
        {
            Types = new []{type, type2, type3};
        }

    }
}