using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DeukBot4.MessageHandlers.JokeHandling
{
    internal class ThanksJoke : IJokeController
    {
        public string Id => "thanks";
        public string Name => "Claim credit when people say thanks";

        private readonly Regex _thanksMatcher = new Regex(@"(?i)((\b)thanks*(\W|$)+)");
        private readonly string[] _thanksResponses = {
            "You're welcome!", "No problem",
            "It was the least I could do, I always do the least I can do!",
            "Yes, well, we'd be absolutely nowhere without me.",
            "Yeah no problem, I'm the only one who does work around here anyway",
            "I gotchu, bb."
        };
        private static int _pos = 0;

        public async Task Run(ReceivedMessage message)
        {
            var content = message.Message.Content;
            if (_thanksMatcher.IsMatch(content))
            {
                if (message.IsHandled)
                    return;
                message.IsHandled = true;
                var response = _thanksResponses[_pos];
                _pos = (_pos + 1) % _thanksResponses.Length;
                message.Message.Channel.SendMessageAsync(response);
            }
        }
    }
}