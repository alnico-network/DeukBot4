using System.Linq;
using System.Threading.Tasks;
using DeukBot4.APIHandlers;
using DeukBot4.Utilities;

namespace DeukBot4.MessageHandlers.JokeHandling
{
    internal class DeltaJoke : IJokeController
    {
        public string Id => "delta";
        public string Name => "No deltas, it's called Origin";
        public async Task Run(ReceivedMessage message)
        {
            var lowerCasedContent = message.Message.Content.ToLowerInvariant().RemoveSpecialCharacters();
            if (lowerCasedContent.Contains("origin"))
                return;

            if (message.IsHandled)
                return;
            var lowerSplit = lowerCasedContent.Split(' ');
            if (lowerSplit.Select(s => Lehvenstein.LevenshteinDistance(s, "delta")).Any(diff => diff <= 1))
            {
                if (message.IsHandled)
                    return;
                message.IsHandled = true;
                var warning = "uhh excuse me it's called\n Origin and it's an art";
                await message.Message.Channel.SendFileAsync(await CatPicHandler.GetCatPicture(warning), "cat_pic.png");
            }
        }
    }
}