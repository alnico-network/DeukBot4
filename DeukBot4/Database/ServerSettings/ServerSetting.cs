﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DeukBot4.Utilities;
using Npgsql;

namespace DeukBot4.Database.ServerSettings
{
    public class ServerSetting
    {
        public ServerSetting(ulong serverId, ulong mutedRoleId = 0, List<string> enabledJokes = null)
        {
            ServerId = serverId;
            MutedRoleId = mutedRoleId;
            EnabledJokes = enabledJokes ?? new List<string>();
        }

        public ulong ServerId { get; }
        public ulong MutedRoleId { get; private set; }
        public List<string> EnabledJokes { get; private set; }

        public async Task SetMutedRoleId(ulong id)
        {
            MutedRoleId = id;
            using (var conn = new DatabaseConnection())
            {
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "UPDATE server_settings SET muted_role = @val WHERE server_id = @key";
                    cmd.Parameters.AddWithValue("val", id.ToLong());
                    cmd.Parameters.AddWithValue("key", ServerId.ToLong());
                    await cmd.ExecuteNonQueryAsync();
                }
            }
        }

        public async Task SetEnabledJokes(List<string> enabledJokes)
        {
            EnabledJokes = enabledJokes;
            using (var conn = new DatabaseConnection())
            {
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection  = conn;
                    cmd.CommandText = "UPDATE server_settings SET enabled_jokes = @val WHERE server_id = @key";
                    cmd.Parameters.AddWithValue("val", string.Join(",", enabledJokes));
                    cmd.Parameters.AddWithValue("key", ServerId.ToLong());
                    await cmd.ExecuteNonQueryAsync();
                }
            }
        }
    }
}