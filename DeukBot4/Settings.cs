﻿using System.Collections.Generic;
using System.IO;
using DeukBot4.MessageHandlers;
using Newtonsoft.Json;

namespace DeukBot4
{
    public class Settings
    {
        [JsonProperty]
        public string Token { get; private set; }
        [JsonProperty]
        public string Username { get; private set; }
        [JsonProperty]
        public ulong OwnerId { get; private set; }
        [JsonProperty]
        public string DatabaseConnectionString { get; private set; }
        [JsonProperty]
        public List<ServerChannelIDs> BackupChannels { get; private set; }
        [JsonProperty]
        public ImageBackupHandler.WebDavSettings WebDavSettings { get; private set; }
        [JsonProperty]
        public ulong DmChannel { get; private set; }


        public static Settings FromJsonFile(string filepath)
        {
            var s = File.ReadAllText(filepath);
            return JsonConvert.DeserializeObject<Settings>(s);
        }
    }
}